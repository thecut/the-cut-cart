# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse_lazy
from django.views import generic
from thecut.cart.forms import CartItemForm
from thecut.cart.models import CartItem


class EditMixin(object):
    model = CartItem
    template_name = 'cart/cartitem_form.html'
    slug_field = 'pk'
    slug_url_kwarg = 'cartitem_pk'
    success_url = reverse_lazy('cart:cart_detail')
    form_class = CartItemForm


class CreateView(EditMixin, generic.CreateView):

    pass


class UpdateView(EditMixin, generic.UpdateView):

    pass


class DeleteView(EditMixin, generic.DeleteView):

    context_object_name = 'item'
    template_name = 'cart/cartitem_delete.html'
