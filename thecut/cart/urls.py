# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import include, patterns, url
from thecut.cart.views import cart, cartitem, order


urls = patterns(

    'thecut.cart.views',

    # Cart
    url(r'^$', cart.DetailView.as_view(), name='cart_detail'),

    # Order
    url(r'^order$', order.CreateView.as_view(), name='order_add'),

    # CartItem
    url(r'^add$', cartitem.CreateView.as_view(),
        name='cartitem_add'),
    url(r'^(?P<cartitem_pk>\d+)/edit$', cartitem.UpdateView.as_view(),
        name='cartitem_edit'),
    url(r'^(?P<cartitem_pk>\d+)/delete$', cartitem.DeleteView.as_view(),
        name='cartitem_delete'),

)


urlpatterns = patterns('', (r'^', include(urls, namespace='cart')))
