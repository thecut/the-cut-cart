# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin
from thecut.cart.models import Cart, CartItem, Order


class CartItemInline(admin.TabularInline):

    extra = 1
    model = CartItem


class CartAdmin(admin.ModelAdmin):

    list_display = ['user', 'created_at', 'updated_at']
    inlines = [CartItemInline]


admin.site.register(Cart, CartAdmin)


class CartItemAdmin(admin.ModelAdmin):

    list_display = ['created_at', 'updated_at']


admin.site.register(CartItem, CartItemAdmin)


class OrderAdmin(admin.ModelAdmin):

    pass


admin.site.register(Order, OrderAdmin)
