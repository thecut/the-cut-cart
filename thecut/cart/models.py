# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from thecut.cart import settings
from thecut.publishing.models import PublishableResource


try:
    from django.contrib.contenttypes.fields import GenericForeignKey
except ImportError:
    from django.contrib.contenttypes.generic import GenericForeignKey


class AbstractCart(models.Model):

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    """:py:class:`datetime` for when this object was first created."""

    updated_at = models.DateTimeField(auto_now=True, editable=False)
    """:py:class:`datetime` for when this object was last saved."""

    class Meta(object):
        abstract = True
        ordering = ['-id']
        get_latest_by = 'created_at'


class Cart(AbstractCart):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True,
                             editable=False)
    """ :py:class:`~django.contrib.auth.models.User`.

    `user` is not required as Cart may be filled while stateless.
    """

    pass


class AbstractCartItem(models.Model):

    """Generic item added to cart."""

    quantity = models.IntegerField(default=1)
    net_value = models.DecimalField(decimal_places=2, max_digits=9,
                                    null=True, blank=True)
    tax_value = models.DecimalField(decimal_places=2, max_digits=9,
                                    null=True, blank=True)
    total_value = models.DecimalField(decimal_places=2, max_digits=9,
                                      null=True, blank=True)
    """:py:class:`unicode` listed price value.

    Null: ``True``."""

    # Generic relation to object.
    content_text = models.CharField(max_length=128, editable=False)
    """:py:class:`unicode` reference.

    This is a record of the cart content object in case the generically related
    app is ever deleted.

    Null: ``False``."""

    content_type = models.ForeignKey('contenttypes.ContentType',
                                     editable=False)
    object_id = models.IntegerField(db_index=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    """:py:class:`datetime` for when this object was first created."""

    updated_at = models.DateTimeField(auto_now=True, editable=False)
    """:py:class:`datetime` for when this object was last saved."""

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.content_text = '{}'.format(self.content_object)
        super(AbstractCartItem, self).save(*args, **kwargs)

    def __str__(self):
        return self.content_text

    def check_positive_quantity(self):
        if self.quantity < 0:
            self.quantity = 0
            self.save()
        return


class CartItem(AbstractCartItem):

    cart = models.ForeignKey('cart.Cart', editable=False)

    class Meta(AbstractCartItem.Meta):
        unique_together = ['cart', 'content_type', 'object_id']

    def get_absolute_url(self):
        return reverse('cart:product_detail', kwargs={'slug': self.slug})

    def get_cartitem(self, obj=None, cart=None):
        if not cart:
            cart = self.cart
        if not obj:
            obj = self.content_object
        try:
            content_type = ContentType.objects.get_for_model(obj)
            return CartItem.objects.get(cart__pk=cart.pk,
                                        content_type=content_type,
                                        object_id=obj.id)
        except CartItem.DoesNotExist:
            return False

    def save(self, *args, **kwargs):
        """Requires `cart` to already exist.

        Until stateless cart creation requires `user`
        """
        # If this `content_object` already exists in this `Cart` add to the
        # quantity rather than creating new object.
        item = self.get_cartitem()
        if item:
            item.quantity = self.quantity
            self = item

        super(CartItem, self).save(*args, **kwargs)


class AbstractOrder(PublishableResource):

    """Converting the Cart in to a placed Order.

    Cart is converted to actual provision good/service.
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    """ :py:class:`~django.contrib.auth.models.User`.

    `user` is required for Order (whereas in Cart `user` is optional).
    """

    order_reference = models.CharField(max_length=64, blank=True)

    class Meta(PublishableResource.Meta):

        abstract = True


class Order(AbstractOrder):

    cart = models.OneToOneField('cart.Cart')

    class Meta(AbstractOrder.Meta):

        permissions = (('read_order', 'Can read order'),)
