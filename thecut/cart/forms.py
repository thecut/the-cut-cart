# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from thecut.cart.models import Cart, CartItem, Order


class CartItemForm(forms.ModelForm):

    class Meta(object):
        model = CartItem


class OrderForm(forms.ModelForm):

    class Meta(object):
        model = Order
        fields = ['order_reference', 'cart']

    def __init__(self, user, **kwargs):
        super(OrderForm, self).__init__(**kwargs)
        self.fields['cart'].queryset = self.get_cart_queryset()

    def get_cart_queryset(self):
        return Cart.objects.all()
