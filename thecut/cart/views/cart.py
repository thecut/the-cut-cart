# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.http import Http404
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.cache import never_cache
from thecut.cart.forms import OrderForm
from thecut.cart.models import Cart


class DetailView(generic.DetailView):

    model = Cart

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        context['form'] = OrderForm
        return context

    def get_queryset(self):
        queryset = super(DetailView, self).get_queryset()
        return queryset.filter(user=self.request.user)

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super(DetailView, self).get_form_kwargs(
            *args, **kwargs)
        form_kwargs.update({'user': self.request.user,
                            'cart': self.object})
        return form_kwargs

    def get_object(self):
        """@@TD: allow cart to be viewed while stateless."""
        try:
            return self.get_queryset()[0]
        except IndexError:
            raise Http404
