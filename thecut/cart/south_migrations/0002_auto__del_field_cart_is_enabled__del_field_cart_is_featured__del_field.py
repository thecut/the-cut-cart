# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
from thecut.authorship.settings import AUTH_USER_MODEL


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Cart.is_enabled'
        db.delete_column(u'cart_cart', 'is_enabled')

        # Deleting field 'Cart.is_featured'
        db.delete_column(u'cart_cart', 'is_featured')

        # Deleting field 'Cart.updated_by'
        db.delete_column(u'cart_cart', 'updated_by_id')

        # Deleting field 'Cart.updated_at'
        db.delete_column(u'cart_cart', 'updated_at')

        # Deleting field 'Cart.publish_at'
        db.delete_column(u'cart_cart', 'publish_at')

        # Deleting field 'Cart.publish_by'
        db.delete_column(u'cart_cart', 'publish_by_id')

        # Deleting field 'Cart.created_at'
        db.delete_column(u'cart_cart', 'created_at')

        # Deleting field 'Cart.created_by'
        db.delete_column(u'cart_cart', 'created_by_id')

        # Deleting field 'Cart.expire_at'
        db.delete_column(u'cart_cart', 'expire_at')

        # Deleting field 'CartItem.is_enabled'
        db.delete_column(u'cart_cartitem', 'is_enabled')

        # Deleting field 'CartItem.is_featured'
        db.delete_column(u'cart_cartitem', 'is_featured')

        # Deleting field 'CartItem.publish_by'
        db.delete_column(u'cart_cartitem', 'publish_by_id')

        # Deleting field 'CartItem.updated_by'
        db.delete_column(u'cart_cartitem', 'updated_by_id')

        # Deleting field 'CartItem.created_by'
        db.delete_column(u'cart_cartitem', 'created_by_id')

        # Deleting field 'CartItem.publish_at'
        db.delete_column(u'cart_cartitem', 'publish_at')

        # Deleting field 'CartItem.expire_at'
        db.delete_column(u'cart_cartitem', 'expire_at')


    def backwards(self, orm):
        # Adding field 'Cart.is_enabled'
        db.add_column(u'cart_cart', 'is_enabled',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Cart.is_featured'
        db.add_column(u'cart_cart', 'is_featured',
                      self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True),
                      keep_default=False)

        # Adding field 'Cart.updated_by'
        db.add_column(u'cart_cart', 'updated_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=datetime.datetime(2013, 11, 22, 0, 0), related_name=u'+', to=orm[AUTH_USER_MODEL]),
                      keep_default=False)

        # Adding field 'Cart.updated_at'
        db.add_column(u'cart_cart', 'updated_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.datetime(2013, 11, 22, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Cart.publish_at'
        db.add_column(u'cart_cart', 'publish_at',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, db_index=True),
                      keep_default=False)

        # Adding field 'Cart.publish_by'
        db.add_column(u'cart_cart', 'publish_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', null=True, to=orm[AUTH_USER_MODEL], blank=True),
                      keep_default=False)

        # Adding field 'Cart.created_at'
        db.add_column(u'cart_cart', 'created_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 11, 22, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Cart.created_by'
        db.add_column(u'cart_cart', 'created_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=datetime.datetime(2013, 11, 22, 0, 0), related_name=u'+', to=orm[AUTH_USER_MODEL]),
                      keep_default=False)

        # Adding field 'Cart.expire_at'
        db.add_column(u'cart_cart', 'expire_at',
                      self.gf('django.db.models.fields.DateTimeField')(blank=True, null=True, db_index=True),
                      keep_default=False)

        # Adding field 'CartItem.is_enabled'
        db.add_column(u'cart_cartitem', 'is_enabled',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'CartItem.is_featured'
        db.add_column(u'cart_cartitem', 'is_featured',
                      self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True),
                      keep_default=False)

        # Adding field 'CartItem.publish_by'
        db.add_column(u'cart_cartitem', 'publish_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', null=True, to=orm[AUTH_USER_MODEL], blank=True),
                      keep_default=False)

        # Adding field 'CartItem.updated_by'
        db.add_column(u'cart_cartitem', 'updated_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=datetime.datetime(2013, 11, 22, 0, 0), related_name=u'+', to=orm[AUTH_USER_MODEL]),
                      keep_default=False)

        # Adding field 'CartItem.created_by'
        db.add_column(u'cart_cartitem', 'created_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name=u'+', to=orm[AUTH_USER_MODEL]),
                      keep_default=False)

        # Adding field 'CartItem.publish_at'
        db.add_column(u'cart_cartitem', 'publish_at',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, db_index=True),
                      keep_default=False)

        # Adding field 'CartItem.expire_at'
        db.add_column(u'cart_cartitem', 'expire_at',
                      self.gf('django.db.models.fields.DateTimeField')(blank=True, null=True, db_index=True),
                      keep_default=False)


    models = {
        AUTH_USER_MODEL: {
            'Meta': {'object_name': AUTH_USER_MODEL.split('.')[-1]},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'cart.cart': {
            'Meta': {'object_name': 'Cart'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.Profile']", 'null': 'True', 'blank': 'True'})
        },
        u'cart.cartitem': {
            'Meta': {'unique_together': "([u'content_type', u'object_id'],)", 'object_name': 'CartItem'},
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cart.Cart']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nett_value': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'tax_value': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'total_value': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '2', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'cart.order': {
            'Meta': {'object_name': 'Order'},
            'cart': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cart.Cart']", 'unique': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['accounts.Profile']"}),
            'expire_at': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'publish_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'publish_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'to': u"orm['accounts.Profile']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': u"orm['accounts.Profile']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.Profile']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['cart']