# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.views import generic
from thecut.cart.forms import OrderForm
from thecut.cart.models import Order


class DetailView(generic.DetailView):

    model = Order


class CreateView(generic.CreateView):

    model = Order
    form_class = OrderForm
